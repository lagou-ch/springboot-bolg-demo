package com.ch.blogdemo.service.impl;

import com.ch.blogdemo.entity.Article;
import com.ch.blogdemo.repository.ArticleRepository;
import com.ch.blogdemo.service.IArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Administrator
 */
@Service
public class ArticleServiceImpl implements IArticleService {

    @Autowired
    private ArticleRepository articleRepository;


    @Override
    public Page<Article> listUser(Pageable pageable) {
        return articleRepository.findAll(pageable);
    }
}
