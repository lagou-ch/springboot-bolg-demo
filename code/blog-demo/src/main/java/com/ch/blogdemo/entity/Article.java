package com.ch.blogdemo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Article
 *
 * @author Administrator
 */
@Entity
@Table(name = "t_article")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Article implements Serializable {
	private static final long serialVersionUID = 1L;
	//必填id
	@Id
	private Long id;
	private String title;//标题
	private String content;//⽂章具体内容

	private Date created;//创建时间

	private Date modified;//修改时间
	private String categories;//⽂章分类
	private String tags;//标签
	@Column(name = "allow_comment")
	private Integer allowComment;// 是否允许评论
	private String thumbnail;// 文章缩略图
}
