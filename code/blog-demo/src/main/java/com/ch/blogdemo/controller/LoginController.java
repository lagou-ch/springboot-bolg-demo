package com.ch.blogdemo.controller;


import com.ch.blogdemo.service.IArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 *
 * 登录跳转
 *
 * @author Administrator
 */
@Controller
public class LoginController {

    @Autowired
    private IArticleService articleService;


    @PostMapping(value = "/user/login")
    public void login(@RequestParam("username") String username,
                      @RequestParam("password") String password, HttpServletResponse resp,
                      ModelMap map) throws IOException {

        if("admin".equals(password) && "admin".equals(username)){
            resp.sendRedirect("/articleList");
            return;
        }
        resp.sendRedirect("/blog-error");
    }

    @RequestMapping(value = "/blog-error")
    public String forwardToError(Model model) {
        model.addAttribute("msg","用户名密码错误");
        return "login";
    }
}