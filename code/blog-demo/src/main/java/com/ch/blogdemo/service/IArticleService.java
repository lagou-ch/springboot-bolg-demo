package com.ch.blogdemo.service;

import com.ch.blogdemo.entity.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IArticleService {

    Page<Article> listUser(Pageable pageable);
}
