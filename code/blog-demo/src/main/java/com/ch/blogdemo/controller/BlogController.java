package com.ch.blogdemo.controller;

import com.ch.blogdemo.entity.Article;
import com.ch.blogdemo.repository.ArticleRepository;
import com.ch.blogdemo.service.IArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @author Administrator
 */
@Controller
public class BlogController {

    @Autowired
    private IArticleService articleService;

    @RequestMapping(value = "/articleList")
    public String articleList(HttpServletRequest request, HttpServletResponse response,Integer pageNum ,ModelMap map) {
        if(pageNum == null){
            pageNum=0;
        }
        if (pageNum < 0) {
            pageNum = 0;
        }
        Sort sort = Sort.by(Sort.Direction.DESC, "modified");
        Pageable pageable= PageRequest.of(pageNum, 2, sort);
        map.addAttribute("articleList", articleService.listUser(pageable));
        return "/client/index";
    }
}
